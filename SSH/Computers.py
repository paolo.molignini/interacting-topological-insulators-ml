#!/usr/bin/env python
##########################################################################################
# This python module calculates the quantities stored in the Assemblers.py module for a
# different values of the tuning parameters.
###########################################################################################


##########################
######## IMPORTS #########
##########################
###########################################################################################
# general python packages:
from __future__ import division
from __future__ import print_function
import sys
import os
import numpy as np
import scipy.integrate as integrate

# custom modules:
from .Assemblers import phik, curv_func
###########################################################################################



#################################
########    METADATA    #########
#################################
###########################################################################################
__author__      =   "Paolo Molignini"
__copyright__   =   "Copyright 2021, University of Cambridge"
__version__     =   "1.0.1"
###########################################################################################







####################################
######     GLOBAL OBJECTS     ######
####################################
###########################################################################################
###########################################################################################




################################
######      COMPUTERS     ######
################################
######################################################################################
def compute_curv_func(t, deltat, V, k_left, k_right, N):
    """
        Computes the curvature function for the SSH model over the whole Brillouin zone.

        Parameters
        ----------
        t: float, hopping parameter.

        deltat: float, staggered hopping parameter.

        V: float, interaction strength.

        k_left: float, lower bound for the k values.

        k_right: float, upper bound for the k values.

        N: integer, number of points in the momentum sum.

        Returns
        -------
        curv_func_array: numpy array, curvature function at all k points for given parameters.

        References
        ----------

        Notes
        -----

        This was mainly used to plot the curvature function to verify that it was behaving as expected.

        Examples
        --------

    """

    # initialization:
    curv_func_array = np.zeros(shape=(N))
    k_array = np.linspace(k_left,k_right,N)

    # output some information on screen:
    print('\n'+"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    print("Working on point (delta_t, V)=("+str(deltat)+","+str(V)+").")
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"+'\n')

    # iterate over all k-points to construct the full curvature function
    for k_idx,k in enumerate(k_array):
        curv_func_array[k_idx] = curv_func(t, deltat, V, k)
        if k_idx%64==0:
            print(k_idx/len(k_array)*100, "% of calculation of curvature function completed.")

    # Saving data to a local folder
    data = {'curv_func': curv_func_array}
    np.save("data/SSH/SSH-curv-func-deltat-{0}-V-{1}-k-{2}-{3}-{4}.npy".format(deltat, V, k_left, k_right, N), data)

    return curv_func_array
######################################################################################
######################################################################################
def compute_phik(t, deltat, V, k_left, k_right, N):
    """
        Computes the angle function phi(k) over the whole Brillouin zone.

        Parameters
        ----------
        t: float, hopping parameter.

        deltat: float, staggered hopping parameter.

        V: float, interaction strength.

        k_left: float, lower bound for the k values.

        k_right: float, upper bound for the k values.

        N: integer, number of points in the momentum sum.

        Returns
        -------
        curv_func_array: numpy array, curvature function at all k for given parameters

        References
        ----------

        Notes
        -----

        This was mainly used to plot the angle function to verify that it was behaving as expected.


        Examples
        --------

    """

    #initialization:
    phik_array = np.zeros(shape=(N))
    k_array = np.linspace(k_left,k_right,N)

    print("Working on point (delta_t, V)=("+str(deltat)+","+str(V)+").")

    # iterate over all k-points to construct the angle function
    for k_idx,k in enumerate(k_array):
        phik_array[k_idx] = phik(t, deltat, V, k)
        print(k_idx/len(k_array)*100, "% of calculation of curvature function completed.")

    #saving data:
    data = {'phik': phik_array}
    np.save("data/SSH/SSH-phik-deltat-{0}-V-{1}-k-{2}-{3}-{4}.npy".format(deltat, V, k_left, k_right, N), data)

    return phik_array
######################################################################################
######################################################################################
def compute_top_inv(t, deltat, V, N, save):
    """
        Computes the topological invariant of the SSH model for the given parameters.

        Parameters
        ----------
        t: float, hopping parameter.

        deltat: float, staggered hopping parameter.

        V: float, interaction strength.

        N: integer, number of points in the momentum sum.

        save: boolean, flag to save the data.

        Returns
        -------
        top_inv: float, topological invariant

        References
        ----------

        Notes
        -----

        Examples
        --------

    """

    # First calculate the (discretized) curvature function for all k-points in the given interval:
    curv_func_array = compute_curv_func(t, deltat, V, 0.0, 2*np.pi, N)
    k_step = 2*np.pi/N
    # Sum over all momenta to approximate the integral:
    top_inv = np.sum(curv_func_array)*k_step

    # Saving data:
    if save==True:
        data = {'top_inv': top_inv}
        np.save("data/SSH/SSH-top-inv-deltat-{0}-V-{1}-N-{2}.npy".format(deltat, V, N), data)

    return top_inv
######################################################################################
######################################################################################
def compute_phase_diag(t, deltat_start, deltat_end, deltat_points, V_start, V_end, V_points, N, save):
    """
        Computes the topological invariant of the SSH model across a range of parameters to yield a phase diagram.

        Parameters
        ----------
        t: float, hopping parameter.

        deltat_start: float, smallest staggered hopping  term in the phase diagram

        deltat_end: float, largest staggered hopping  term in the phase diagram

        deltat_points: integer, number of points in the deltat-direction

        V_start: float, smallest interaction in the phase diagram

        V_end: float, largest interaction in the phase diagram

        V_points: integer, number of points in the M-direction

        N: integer, number of points in the momentum sum.

        save: boolean, flag to save the data at each point

        Returns
        -------
        top_inv_array: numpy array, array for the topological invariant in parameter space

        References
        ----------

        Notes
        -----

        Examples
        --------

    """

    # Initialization:
    top_inv_array = np.zeros(shape=(V_points, deltat_points))
    deltat_array = np.linspace(deltat_start, deltat_end, deltat_points)
    V_array = np.linspace(V_start, V_end, V_points)

    # Loop over the parameters (deltat, V) defining the phase diagram and calculate
    # the topological invariant for each pair of values:
    for deltat_idx, deltat in enumerate(deltat_array):
        for V_idx, V in enumerate(V_array):
            top_inv_array[V_idx][deltat_idx] = compute_top_inv(t, deltat, V, N, save)

    # Saving data:
    data = {'top_inv_array': top_inv_array}
    np.save("data/SSH/SSH-phase-diag-deltat-{0}-{1}-{2}-V-{3}-{4}-{5}-N-{6}.npy".format(deltat_start, deltat_end, deltat_points, V_start, V_end, V_points, N), data)

    return top_inv_array
######################################################################################




######################################################################################
def get_training_set(t, deltat_initial, deltat_final, deltat_points, random_flag):
    """
        Collects the values of the curvature function at the high-symmetry point k=pi to use for the training of the NN.

        Parameters
        ----------
        t: float, hopping term in the Hamiltonian.

        deltat_initial: float, initial value in the range for the staggered hopping term in the Hamiltonian.

        deltat_final: float, final value in the range for the staggered hopping term in the Hamiltonian.

        deltat_points: integer, total number of points in the range for the staggered hopping term in the Hamiltonian.

        random_flag: boolean, flag to randomly sample points in the deltat interval.

        Returns
        -------
        curv_func_array: numpy array, curvature function at the HSP for given parameters

        References
        ----------

        Notes
        -----
        This function only generates non-interacting data (V=0).

        Examples
        --------

    """

    # Initialization of the delta array (uniformly random or linearly spaced points):
    if random_flag==True:
        deltat_array=np.random.uniform(deltat_initial, deltat_final, deltat_points)
    else:
        deltat_array=np.linspace(deltat_initial, deltat_final, deltat_points)
    curv_func_array=np.zeros(shape=(deltat_points, 1))
    top_inv_array=np.zeros(deltat_points)

    #loop over all points to get the values of the curvature function:
    for deltat_idx, deltat in enumerate(deltat_array):
        k=1.001*np.pi    # The value of the HSP is chosen slightly away from pi to avoid numerical singularities
        curv_func_array[deltat_idx] = curv_func(t, deltat, 0, k)
        # Print status of the computation:
        if deltat_idx%64==0:
            print(deltat_idx/len(deltat_array)*100, "% of calculation of curvature function completed.")

        #get labels (topological invariant) analytically:
        if deltat > 0:
            top_inv_array[deltat_idx]=0
        elif deltat < 0:
            top_inv_array[deltat_idx]=1
        else:
            top_inv_array[deltat_idx]=0

    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    print("Data successfully generated.")
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!")

    # Saving data:
    data = {'curv_func': curv_func_array, 'top_inv': top_inv_array}
    if random_flag==True:
        np.save("data/SSH/SSH-curv-func-top-inv-deltat-{0}-{1}-{2}-rand.npy".format(deltat_initial, deltat_final, deltat_points), data)
    if random_flag==False:
        np.save("data/SSH/SSH-curv-func-top-inv-deltat-{0}-{1}-{2}.npy".format(deltat_initial, deltat_final, deltat_points), data)

    return curv_func_array, top_inv_array
######################################################################################

######################################################################################
def get_test_set(t, deltat_initial, deltat_final, deltat_points, V_initial, V_final, V_points):
    """
        Collects the values of the curvature function at the high-symmetry point k=pi to use to test the NN (interacting case).

        Parameters
        ----------
        t: float, hopping term in the Hamiltonian.

        deltat_initial: float, initial value in the range for the staggered hopping term in the Hamiltonian.

        deltat_final: float, final value in the range for the staggered hopping term in the Hamiltonian.

        deltat_points: integer, total number of points in the range for the staggered hopping term in the Hamiltonian.

        V_initial: float, initial value in the range for the interaction strength in the Hamiltonian.

        V_final: float, final value in the range for the interaction strength in the Hamiltonian.

        V_points: integer, total number of points in the range for the interaction strength in the Hamiltonian.

        Returns
        -------
        curv_func_array: numpy array, curvature function at the HSPs for given parameters

        References
        ----------

        Notes
        -----
        deltat_points should always be even to avoid sampling M=0 (training set).


        Examples
        --------

    """

    if deltat_points%2==1:
        print("You have chosen an odd number of points in the deltat interval, but this samples the deltat=0 line, which corresponds to the non-interacting case. As a consequence, the test and training sets are not disjoint. Please choose an even number of points.")
        sys.exit()

    # Initialization of the arrays:
    deltat_array=np.linspace(deltat_initial, deltat_final, deltat_points)
    V_array=np.linspace(V_initial, V_final, V_points)
    curv_func_array=np.zeros(shape=(V_points, deltat_points)) #Note: all parameters are equivalent "test points"

    # Loop over all points to get the values of the curvature function:
    for deltat_idx, deltat in enumerate(deltat_array):
        for V_idx, V in enumerate(V_array):
            k=1.001*np.pi   # The value of the HSP is chosen slightly away from pi to avoid numerical singularities
            curv_func_array[V_idx][deltat_idx] = curv_func(t, deltat, V, k)
        if deltat_idx%4 == 0:
            print((deltat_idx*len(V_array) + V_idx)/(len(deltat_array)*len(V_array))*100, "% of calculation of curvature function completed.")

    # Saving data:
    data = {'curv_func': curv_func_array}
    np.save("data/SSH/SSH-curv-func-deltat-{0}-{1}-{2}-V-{3}-{4}-{5}.npy".format(deltat_initial,deltat_final,deltat_points,V_initial,V_final,V_points), data)

    return curv_func_array
######################################################################################
