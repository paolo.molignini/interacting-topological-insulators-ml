#!/usr/bin/env python
##########################################################################################
# This python program uses the keras library to perform phase classification of
# topological phases of an interacting SSH model.
###########################################################################################


##########################
######## IMPORTS #########
##########################
###########################################################################################
# general python packages:
from __future__ import division
from __future__ import print_function
import sys
import numpy as np
import scipy.integrate as integrate

#plotting
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib import rc
from matplotlib import colors
import colormaps as cmaps

#machine learning packages (keras and tensorflow)
import tensorflow as tf
import tensorflow.keras
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.utils import plot_model
from tensorflow.keras.layers import Dense, Dropout,Conv2D,MaxPooling2D,Flatten, BatchNormalization, LeakyReLU, Conv2DTranspose,AveragePooling2D
from tensorflow.keras.optimizers import RMSprop
from tensorflow.keras import regularizers

# custom modules:
from .Assemblers import *
from .Computers import *
###########################################################################################





#################################
########    METADATA    #########
#################################
###########################################################################################
__author__      =   "Paolo Molignini"
__copyright__   =   "Copyright 2021, University of Cambridge"
__version__     =   "1.0.1"
###########################################################################################


######################################################################################
def train_ANN(M_initial, M_final, M_points, width, epochs, batch_size, random_flag=True, shuffle=True, load=False):
    """
        Computes the topological invariant across a range of parameters to yield a phase diagram.
       
        Parameters
        ----------
        M_initial: float, smallest mass term in the phase diagram

        M_final: float, largest mass term in the phase diagram

        M_points: integer, number of points in the M-direction

        width: integer, number of neurons in the hidden layer.

        epochs: integer, number of epochs the network will be trained for.

        batch_size: integer, size of the batches fed to the neural network.
        
        random_flag: boolean, flag for using random points in the M interval for training.

        shuffle: boolean, flag for shuffling the training data.
    
        load: boolean, flag to load previously computed training data.

        Returns
        -------
        model: keras model, the model trained with the provided data from the M-interval.
           
        References
        ----------

        Notes
        -----

        Empirically we find that no regularizers give better performance.

        Examples
        --------

    """
    

    np.set_printoptions(threshold=sys.maxsize)
    
    if load==True:
        if random_flag==True:
            data = np.load("data/Chern/curv-func-HSPs-top-inv-M-{0}-{1}-{2}-rand.npy".format(M_initial,M_final,M_points), allow_pickle=True, encoding='latin1')
        else:
            data = np.load("data/Chern/curv-func-HSPs-top-inv-M-{0}-{1}-{2}.npy".format(M_initial,M_final,M_points), allow_pickle=True, encoding='latin1')

        train_features  = data[()]['curv_func']
        train_labels  = data[()]['top_inv']
    
    elif load==False:
        train_features, train_labels = get_training_set_all_HSPs(M_initial, M_final, M_points, random_flag)
    
    
#    print(np.shape(train_features))
#    print(np.shape(train_labels))
    print("train features:", train_features)
    print("train labels:",train_labels)

    # Prepare data for training:
    #1) shuffle data with the same sequence.
    #turn label vector into a 1xN matrix to enable simultaneous shuffling:
    train_labels = np.array([train_labels])
    if shuffle==True:
        #concatenate data:
        full_data = np.concatenate((train_features,train_labels.T),axis=1)
        print("full data:"+"\n")
        print(full_data)
        #shuffling:
        np.random.seed()
        np.random.shuffle(full_data)
        #re-split data into feature vector and labels:
        train_features = full_data[:,:-1]
        train_labels = full_data[:,-1:].T
        print("splitting:")
        print(train_features)
        print(train_labels)
    
    #rewrite labels as 3-dim vector for the three phases
    y = []
    for i, label in enumerate(train_labels[0]):
        if label==-1:
            y.append((1, 0, 0))   # phase 1
        elif label==0:
            y.append((0, 1, 0))   # phase 2
        elif label==1:
            y.append((0, 0, 1))   # phase 3

    X = train_features
    y = np.array(y)
    
    # Create model.
    # Network architecture: input layer, hidden layer with width neurons, and output layer with 3 neurons.
    model = Sequential()
    model.add(Dense(width, input_dim=3, kernel_initializer='glorot_normal', activation='sigmoid', kernel_regularizer=regularizers.l2(0.0)))
    model.add(Dense(3, kernel_initializer='glorot_normal', activation='softmax', kernel_regularizer=regularizers.l2(0.0)))

    # Compile model
    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
    model.summary()
    # Train model.
    model.fit(X, y, epochs=epochs, batch_size=batch_size)
    
    #saving model:
    if random_flag==True:
        model.save('data/Chern/trained_model-M-{0}-{1}-{2}-rand'.format(M_initial,M_final,M_points))
    elif random_flag==False:
        model.save('data/Chern/trained_model-M-{0}-{1}-{2}'.format(M_initial,M_final,M_points))


    return model
######################################################################################




######################################################################################
def test_ANN(X_test, y_test, M_points, M_initial_training, M_final_training, M_points_training, u_points, random_flag=True, eval=True):
    """
        Predict the values of the topological invariant with the ANN.
       
        Parameters
        ----------
        X_test: numpy array, test data (features).

        y_test: numpy array, test data (labels).

        M_points: integer, number of points in the M-direction.
        
        M_initial_training: float, initial value of M in the training set.
        
        M_final_training: float, final value of M in the training set.

        M_points_training: int, number of points in the M interval in the training set.
        
        random_flag: boolean, flag for generating randomly distributed points in the training set.

        u_points: integer, number of points in the u-direction.
    
        eval: boolean, flag to generate performance information about the training (works only if sensible labels are provided).

        Returns
        -------
        model: keras model, the model trained with the provided data from the M-interval.
           
        References
        ----------

        Notes
        -----
        
        eval=True gives sensible results only if true labels are provided.
    
        Examples
        --------

    """
    
    # specify here the interval used for the training of the model
    # (the models are saved with this info)
    #load model:
    model = load_model('data/Chern/trained_model-M-{0}-{1}-{2}-rand'.format(M_initial_training,M_final_training,M_points_training))

    # reshape the test data to a structure that can be accepted by the neural network
    # (merge all data points from different M and V in the same dimension).
    X_test_reshaped = X_test.reshape(M_points*u_points,3)
    y_test_reshaped = y_test.reshape(M_points*u_points,3)

    #Evaluate model.
    predictions = model.predict(X_test_reshaped)
    predictions_reshaped = predictions.reshape(u_points,M_points,3)
    
    if eval==True:
        #obtain performance data:
        preds = model.evaluate(x=X_test_reshaped, y=y_test_reshaped)
        print ("Loss = {0:.3f}".format(preds[0]))
        print ("Test Accuracy = {0:.3f}".format(preds[1]))
        return predictions_reshaped, preds

    else:
        return predictions_reshaped
######################################################################################
