#!/usr/bin/env python
##########################################################################################
# This python module is used to generate the data from 1D and 2D topological insulators that
# is fed to the neural network.
###########################################################################################


##########################
######## IMPORTS #########
##########################
###########################################################################################
from __future__ import division
from __future__ import print_function
import numpy as np
import scipy.integrate as integrate
###########################################################################################


#################################
########    METADATA    #########
#################################
###########################################################################################
__author__      =   "Paolo Molignini"
__copyright__   =   "Copyright 2021, University of Cambridge"
__version__     =   "1.0.2"
###########################################################################################


####################################
######     GLOBAL OBJECTS     ######
####################################
###########################################################################################
dk = 10**(-8)*np.pi
###########################################################################################


################################
######     ASSEMBLERS     ######
################################
################################
################################
######################################################################################
######     HAMILTONIAN (NON-INTERACTING PART)
######################################################################################
def d1(kx, ky):
    """
        Assembles first component of vector Hamiltonian.
    
        Parameters
        ----------
        kx: float, momentum in x direction.
        
        ky: float, momentum in y direction.

        Returns
        -------
        d1: float, first component of vector Hamiltonian.
        
        
        References
        ----------
        W. Chen, Weakly interacting topological insulators:
        Quantum criticality and the renormalization group approach,
        PHYSICAL REVIEW B 97, 115130 (2018).

        Notes
        -----

        Examples
        --------

    """
    d1 = np.sin(kx)
    return d1
######################################################################################

######################################################################################
def d2(kx, ky):
    """
        Assembles second component of vector Hamiltonian.
    
        Parameters
        ----------
        kx: float, momentum in x direction.

        ky: float, momentum in y direction.


        Returns
        -------
        d2: float, second component of vector Hamiltonian.
        
        
        References
        ----------
        W. Chen, Weakly interacting topological insulators:
        Quantum criticality and the renormalization group approach,
        PHYSICAL REVIEW B 97, 115130 (2018).

        Notes
        -----

        Examples
        --------

    """
    d2 = np.sin(ky)
    return d2
######################################################################################

######################################################################################
def d3(M, kx, ky):
    """
        Assembles third component of vector Hamiltonian.
    
        Parameters
        ----------
        M: float, mass term.
        
        kx: float, momentum in x direction.
        
        ky: float, momentum in y direction.


        Returns
        -------
        d3: float, third component of vector Hamiltonian.
        
        
        References
        ----------
        W. Chen, Weakly interacting topological insulators:
        Quantum criticality and the renormalization group approach,
        PHYSICAL REVIEW B 97, 115130 (2018).

        Notes
        -----

        Examples
        --------

    """
    d3 = M + 2 - np.cos(kx) - np.cos(ky)
    return d3
######################################################################################

######################################################################################
def dmag(M, kx, ky):
    """
        Assembles magnitude of the vector Hamiltonian.
    
        Parameters
        ----------
        M: float, mass term.
        
        kx: float, momentum in x direction.
        
        ky: float, momentum in y direction.


        Returns
        -------
        d_mag: float, magnitude of vector Hamiltonian.
        
        
        References
        ----------
        W. Chen, Weakly interacting topological insulators:
        Quantum criticality and the renormalization group approach,
        PHYSICAL REVIEW B 97, 115130 (2018).

        Notes
        -----

        Examples
        --------

    """
    d_mag = np.sqrt(np.sin(kx)**2 + np.sin(ky)**2 + (M + 2 - np.cos(kx) - np.cos(ky))**2)
    return d_mag
######################################################################################

######################################################################################
def dkx_dmag(M, kx, ky):
    """
        Assembles the d/dkx derivative of the bare Hamiltonian unit vector (computed analytically).
    
        Parameters
        ----------
        M: float, mass term in the Hamiltonian.
                
        kx: float, momentum in x direction
        
        ky: float, momentum in y direction.
        

        Returns
        -------
        dkx_dmag: float, d/dkx derivative of the bare Hamiltonian unit vector.
        
        
        References
        ----------
        W. Chen, Weakly interacting topological insulators:
        Quantum criticality and the renormalization group approach,
        PHYSICAL REVIEW B 97, 115130 (2018).

        Notes
        -----

        Examples
        --------

    """
    
    dkx_dmag = np.sin(kx)*(M+2-np.cos(ky))/dmag(M,kx,ky)
        
    return dkx_dmag
######################################################################################

######################################################################################
def dky_dmag(M, kx, ky):
    """
        Assembles the d/dky derivative of the bare Hamiltonian unit vector (computed analytically).
    
        Parameters
        ----------
        M: float, mass term in the Hamiltonian.
                
        kx: float, momentum in x direction
        
        ky: float, momentum in y direction.
        

        Returns
        -------
        dky_dmag: float, d/dky derivative of the bare Hamiltonian unit vector.
        
        
        References
        ----------
        W. Chen, Weakly interacting topological insulators:
        Quantum criticality and the renormalization group approach,
        PHYSICAL REVIEW B 97, 115130 (2018).

        Notes
        -----

        Examples
        --------

    """
    
    dky_dmag = np.sin(ky)*(M+2-np.cos(kx))/dmag(M,kx,ky)
        
    return dky_dmag
######################################################################################






######################################################################################
######     SELF ENERGIES / INTERACTING PART
######################################################################################

######################################################################################
def Mofq(qx, qy, u):
    """
        Assembles interaction vertex for electron-phonon interactions.
    
        Parameters
        ----------
        qx: float, momentum in x direction
        
        qy: float, momentum in y direction.

        u: float, acoustic phonon coupling constant.

        Returns
        -------
        vertex: float, vertex
        
        
        References
        ----------
        W. Chen, Weakly interacting topological insulators:
        Quantum criticality and the renormalization group approach,
        PHYSICAL REVIEW B 97, 115130 (2018).
        
        G. D. Mahan, Many-Particle Physics (Springer, 2000).

        Notes
        -----

        Examples
        --------

    """
    vertex = u*np.sqrt(np.sqrt(qx**2+qy**2))
    return vertex
######################################################################################
######################################################################################
def get_self_energies(M, u, vs, kx, ky, omega):
    """
        Calculates the self energies up to one loop for the 2D Chern insulator with electron-phonon interactions. The integration is performed with the scipy.integrate module.
    
        Parameters
        ----------
        M: float, mass term in the Hamiltonian.
        
        u: float, acoustic phonon coupling constant.
        
        vs: float, sound velocity.

        kx: float, momentum in x direction
        
        ky: float, momentum in y direction.
        
        omega: float, Matsubara frequency.


        Returns
        -------
        Sigma_AAmBB: float, difference between the two Hartree self-energy terms.

        Sigma_AApBB: float, negative sum of the two Hartree self-energy terms.

        Re_Sigma_AB: float, real part of the Fock self-energy.
        
        Im_Sigma_AB: float, imaginary part of the Fock self-energy.

        
        References
        ----------
        W. Chen, Weakly interacting topological insulators:
        Quantum criticality and the renormalization group approach,
        PHYSICAL REVIEW B 97, 115130 (2018).
        
        G. D. Mahan, Many-Particle Physics (Springer, 2000).

        Notes
        -----
        

        Examples
        --------

    """
    
    # Definition of ∑AA - ∑BB:
    f1 = lambda y, x: -2/(4*np.pi**2)*Mofq(x,y,u)**2*d3(M,kx-x,ky-y)/dmag(M,kx-x,ky-y)*(vs*np.sqrt(x**2+y**2) + dmag(M,kx-x,ky-y))/(omega**2 + (dmag(M,kx-x,ky-y) + vs*np.sqrt(x**2+y**2))**2)
    Sigma_AAmBB, err = integrate.dblquad(f1, -np.pi, np.pi, lambda x: -np.pi, lambda x: np.pi)

    # Definition of ∑AA + ∑BB:
    f2 = lambda y, x: -2/(4*np.pi**2)*Mofq(x,y,u)**2*omega/(omega**2 + (dmag(M,kx-x,ky-y) + vs*np.sqrt(x**2+y**2))**2)
    Sigma_AApBB, err = integrate.dblquad(f2, -np.pi, np.pi, lambda x: -np.pi, lambda x: np.pi)
    Sigma_AApBB = 1j*(Sigma_AApBB)

    # Definition of Re[∑AB]:
    f3 = lambda y, x: -1/(4*np.pi**2)*Mofq(x,y,u)**2*d1(kx-x,ky-y)/dmag(M,kx-x,ky-y)*(vs*np.sqrt(x**2+y**2) + dmag(M,kx-x,ky-y))/(omega**2 + (dmag(M,kx-x,ky-y) + vs*np.sqrt(x**2+y**2))**2)
    Re_Sigma_AB, err = integrate.dblquad(f3, -np.pi, np.pi, lambda x: -np.pi, lambda x: np.pi)

    # Definition of Im[∑AB]:
    f4 = lambda y, x: 1/(4*np.pi**2)*Mofq(x,y,u)**2*d2(kx-x,ky-y)/dmag(M,kx-x,ky-y)*(vs*np.sqrt(x**2+y**2) + dmag(M,kx-x,ky-y))/(omega**2 + (dmag(M,kx-x,ky-y) + vs*np.sqrt(x**2+y**2))**2)
    Im_Sigma_AB, err = integrate.dblquad(f4, -np.pi, np.pi, lambda x: -np.pi, lambda x: np.pi)

    return Sigma_AAmBB, Sigma_AApBB, Re_Sigma_AB, Im_Sigma_AB
######################################################################################
######################################################################################
def get_self_energies_derivatives(M, u, vs, kx, ky, omega):
    """
        Calculates the derivatives of the self energies up to one loop for the 2D Chern insulator with electron-phonon interactions.
    
        Parameters
        ----------
        M: float, mass term in the Hamiltonian.
        
        u: float, acoustic phonon coupling constant.
        
        vs: float, sound velocity.

        kx: float, momentum in x direction
        
        ky: float, momentum in y direction.
        
        omega: float, Matsubara frequency.


        Returns
        -------
        domega_Sigma_AApBB: float, ∂/∂omega[∑AA+∑BB]
        
        dkx_Sigma_AApBB: float, ∂/∂kx[∑AA+∑BB].
        
        dky_Sigma_AApBB: float, ∂/∂ky[∑AA+∑BB].
        
        domega_Re_Sigma_AB: float, ∂/∂omega[Re[∑AB]].
        
        dkx_Re_Sigma_AB: float, ∂/∂kx[Re[∑AB]].
        
        dky_Re_Sigma_AB: float, ∂/∂ky[Re[∑AB]].
        
        domega_Im_Sigma_AB: float, ∂/∂omega[Im[∑AB]].
        
        dkx_Im_Sigma_AB: float, ∂/∂kx[Im[∑AB]].
        
        dky_Im_Sigma_AB: float, ∂/∂ky[Im[∑AB]].
        
        domega_Sigma_AAmBB: float, ∂/∂omega[∑AA-∑BB].
        
        dkx_Sigma_AAmBB: float, ∂/∂kx[∑AA-∑BB].
        
        dky_Sigma_AAmBB: float, ∂/∂ky[∑AA-∑BB].

        
        References
        ----------
        W. Chen, Weakly interacting topological insulators:
        Quantum criticality and the renormalization group approach,
        PHYSICAL REVIEW B 97, 115130 (2018).


        Notes
        -----
        

        Examples
        --------

    """
    
    #∂omega(Sigma_AA+Sigma_BB):
    f1 = lambda y, x: 2/(4*np.pi**2)*Mofq(x,y,u)**2*(-omega**2 + (dmag(M,kx-x,ky-y) + vs*np.sqrt(x**2+y**2))**2)/(omega**2 + (dmag(M,kx-x,ky-y) + vs*np.sqrt(x**2+y**2))**2)**2
    domega_Sigma_AApBB, err = integrate.dblquad(f1, -np.pi, np.pi, lambda x: -np.pi, lambda x: np.pi)
    domega_Sigma_AApBB = 1j*domega_Sigma_AApBB

    #∂kx(Sigma_AA+Sigma_BB):
    f2 = lambda y, x: 4/(4*np.pi**2)*Mofq(x,y,u)**2*omega/(omega**2 + (dmag(M,kx-x,ky-y) + vs*np.sqrt(x**2+y**2))**2)**2*(dmag(M,kx-x,ky-y)+vs*np.sqrt(x**2+y**2))*dkx_dmag(M,kx-x,ky-y)
    dkx_Sigma_AApBB, err = integrate.dblquad(f2, -np.pi, np.pi, lambda x: -np.pi, lambda x: np.pi)
    dkx_Sigma_AApBB = 1j*dkx_Sigma_AApBB
    
    #∂ky(Sigma_AA+Sigma_BB):
    f3 = lambda y, x: 4/(4*np.pi**2)*Mofq(x,y,u)**2*omega/(omega**2 + (dmag(M,kx-x,ky-y) + vs*np.sqrt(x**2+y**2))**2)**2*(dmag(M,kx-x,ky-y)+vs*np.sqrt(x**2+y**2))*dky_dmag(M,kx-x,ky-y)
    dky_Sigma_AApBB, err = integrate.dblquad(f3, -np.pi, np.pi, lambda x: -np.pi, lambda x: np.pi)
    dky_Sigma_AApBB = 1j*dky_Sigma_AApBB
    
    #∂omega(Sigma_AA-Sigma_BB):
    f4 = lambda y, x: 4/(4*np.pi**2)*Mofq(x,y,u)**2*omega*d3(M,kx-x,ky-y)/dmag(M,kx-x,ky-y)*(vs*np.sqrt(x**2+y**2)+dmag(M,kx-x,ky-y))/(omega**2 + (dmag(M,kx-x,ky-y) + vs*np.sqrt(x**2+y**2))**2)**2
    domega_Sigma_AAmBB, err = integrate.dblquad(f4, -np.pi, np.pi, lambda x: -np.pi, lambda x: np.pi)
    
    #∂kx(Sigma_AA-Sigma_BB):
    f5 = lambda y, x: -2/(4*np.pi**2)*Mofq(x,y,u)**2*(vs*np.sqrt(x**2+y**2)*np.sin(kx-x)/((omega**2+(vs*np.sqrt(x**2+y**2))**2)*dmag(M,kx-x,ky-y) + 2*(vs*np.sqrt(x**2+y**2))*dmag(M,kx-x,ky-y)**2 + dmag(M,kx-x,ky-y)**3) - (vs*np.sqrt(x**2+y**2))*d3(M,kx-x,ky-y)*((omega**2+(vs*np.sqrt(x**2+y**2))**2)*dkx_dmag(M,kx-x,ky-y) + 4*(vs*np.sqrt(x**2+y**2))*dmag(M,kx-x,ky-y)*dkx_dmag(M,kx-x,ky-y) + 3*dmag(M,kx-x,ky-y)**2*dkx_dmag(M,kx-x,ky-y))/((omega**2+(vs*np.sqrt(x**2+y**2))**2)*dmag(M,kx-x,ky-y) + 2*(vs*np.sqrt(x**2+y**2))*dmag(M,kx-x,ky-y)**2 + dmag(M,kx-x,ky-y)**3)**2 + np.sin(kx-x)/(omega**2+(dmag(M,kx-x,ky-y)+(vs*np.sqrt(x**2+y**2)))**2) - 2*d3(M,kx-x,ky-y)*(dmag(M,kx-x,ky-y)+(vs*np.sqrt(x**2+y**2)))*dkx_dmag(M,kx-x,ky-y)/(omega**2+(dmag(M,kx-x,ky-y)+(vs*np.sqrt(x**2+y**2)))**2)**2)
    dkx_Sigma_AAmBB, err = integrate.dblquad(f5, -np.pi, np.pi, lambda x: -np.pi, lambda x: np.pi)
    
    #∂ky(Sigma_AA-Sigma_BB):
    f6 = lambda y, x: -2/(4*np.pi**2)*Mofq(x,y,u)**2*((vs*np.sqrt(x**2+y**2))*np.sin(ky-y)/((omega**2+(vs*np.sqrt(x**2+y**2))**2)*dmag(M,kx-x,ky-y) + 2*(vs*np.sqrt(x**2+y**2))*dmag(M,kx-x,ky-y)**2 + dmag(M,kx-x,ky-y)**3) - (vs*np.sqrt(x**2+y**2))*d3(M,kx-x,ky-y)*((omega**2+(vs*np.sqrt(x**2+y**2))**2)*dky_dmag(M,kx-x,ky-y) + 4*(vs*np.sqrt(x**2+y**2))*dmag(M,kx-x,ky-y)*dky_dmag(M,kx-x,ky-y) + 3*dmag(M,kx-x,ky-y)**2*dky_dmag(M,kx-x,ky-y))/((omega**2+(vs*np.sqrt(x**2+y**2))**2)*dmag(M,kx-x,ky-y) + 2*(vs*np.sqrt(x**2+y**2))*dmag(M,kx-x,ky-y)**2 + dmag(M,kx-x,ky-y)**3)**2 + np.sin(ky-y)/(omega**2+(dmag(M,kx-x,ky-y)+(vs*np.sqrt(x**2+y**2)))**2) - 2*d3(M,kx-x,ky-y)*(dmag(M,kx-x,ky-y)+(vs*np.sqrt(x**2+y**2)))*dky_dmag(M,kx-x,ky-y)/(omega**2+(dmag(M,kx-x,ky-y)+(vs*np.sqrt(x**2+y**2)))**2)**2)
    dky_Sigma_AAmBB, err = integrate.dblquad(f6, -np.pi, np.pi, lambda x: -np.pi, lambda x: np.pi)
    
    #∂omega(Re[Sigma_AB]):
    f7 = lambda y, x: 2/(4*np.pi**2)*Mofq(x,y,u)**2*omega*d1(kx-x,ky-y)/dmag(M,kx-x,ky-y)*((vs*np.sqrt(x**2+y**2))+dmag(M,kx-x,ky-y))/(omega**2 + (dmag(M,kx-x,ky-y) + (vs*np.sqrt(x**2+y**2)))**2)**2
    domega_Re_Sigma_AB, err = integrate.dblquad(f7, -np.pi, np.pi, lambda x: -np.pi, lambda x: np.pi)
    
    #∂kx(Re[Sigma_AB]):
    f8 = lambda y, x: -1/(4*np.pi**2)*Mofq(x,y,u)**2*((vs*np.sqrt(x**2+y**2))*np.cos(kx-x)/((omega**2+(vs*np.sqrt(x**2+y**2))**2)*dmag(M,kx-x,ky-y) + 2*(vs*np.sqrt(x**2+y**2))*dmag(M,kx-x,ky-y)**2 + dmag(M,kx-x,ky-y)**3) - (vs*np.sqrt(x**2+y**2))*d1(kx-x,ky-y)*((omega**2+(vs*np.sqrt(x**2+y**2))**2)*dkx_dmag(M,kx-x,ky-y) + 4*(vs*np.sqrt(x**2+y**2))*dmag(M,kx-x,ky-y)*dkx_dmag(M,kx-x,ky-y) + 3*dmag(M,kx-x,ky-y)**2*dkx_dmag(M,kx-x,ky-y))/((omega**2+(vs*np.sqrt(x**2+y**2))**2)*dmag(M,kx-x,ky-y) + 2*(vs*np.sqrt(x**2+y**2))*dmag(M,kx-x,ky-y)**2 + dmag(M,kx-x,ky-y)**3)**2 + np.cos(kx-x)/(omega**2+(dmag(M,kx-x,ky-y)+(vs*np.sqrt(x**2+y**2)))**2) - 2*d1(kx-x,ky-y)*(dmag(M,kx-x,ky-y)+(vs*np.sqrt(x**2+y**2)))*dkx_dmag(M,kx-x,ky-y)/(omega**2+(dmag(M,kx-x,ky-y)+(vs*np.sqrt(x**2+y**2)))**2)**2)
    dkx_Re_Sigma_AB, err = integrate.dblquad(f8, -np.pi, np.pi, lambda x: -np.pi, lambda x: np.pi)
    
    #∂ky(Re[Sigma_AB]):
    f9 = lambda y, x: -1/(4*np.pi**2)*Mofq(x,y,u)**2*(- (vs*np.sqrt(x**2+y**2))*d1(kx-x,ky-y)*((omega**2+(vs*np.sqrt(x**2+y**2))**2)*dky_dmag(M,kx-x,ky-y) + 4*(vs*np.sqrt(x**2+y**2))*dmag(M,kx-x,ky-y)*dky_dmag(M,kx-x,ky-y) + 3*dmag(M,kx-x,ky-y)**2*dky_dmag(M,kx-x,ky-y))/((omega**2+(vs*np.sqrt(x**2+y**2))**2)*dmag(M,kx-x,ky-y) + 2*(vs*np.sqrt(x**2+y**2))*dmag(M,kx-x,ky-y)**2 + dmag(M,kx-x,ky-y)**3)**2 - 2*d1(kx-x,ky-y)*(dmag(M,kx-x,ky-y)+(vs*np.sqrt(x**2+y**2)))*dky_dmag(M,kx-x,ky-y)/(omega**2+(dmag(M,kx-x,ky-y)+(vs*np.sqrt(x**2+y**2)))**2)**2)
    dky_Re_Sigma_AB, err = integrate.dblquad(f9, -np.pi, np.pi, lambda x: -np.pi, lambda x: np.pi)
    
    #∂omega(Re[Sigma_AB]):
    f10 = lambda y, x: -2/(4*np.pi**2)*Mofq(x,y,u)**2*omega*d2(kx-x,ky-y)/dmag(M,kx-x,ky-y)*((vs*np.sqrt(x**2+y**2))+dmag(M,kx-x,ky-y))/(omega**2 + (dmag(M,kx-x,ky-y) + (vs*np.sqrt(x**2+y**2)))**2)**2
    domega_Im_Sigma_AB, err = integrate.dblquad(f10, -np.pi, np.pi, lambda x: -np.pi, lambda x: np.pi)
    
    #∂kx(Im[Sigma_AB]):
    f11 = lambda y, x: 1/(4*np.pi**2)*Mofq(x,y,u)**2*(- (vs*np.sqrt(x**2+y**2))*d2(kx-x,ky-y)*((omega**2+(vs*np.sqrt(x**2+y**2))**2)*dkx_dmag(M,kx-x,ky-y) + 4*(vs*np.sqrt(x**2+y**2))*dmag(M,kx-x,ky-y)*dkx_dmag(M,kx-x,ky-y) + 3*dmag(M,kx-x,ky-y)**2*dkx_dmag(M,kx-x,ky-y))/((omega**2+(vs*np.sqrt(x**2+y**2))**2)*dmag(M,kx-x,ky-y) + 2*(vs*np.sqrt(x**2+y**2))*dmag(M,kx-x,ky-y)**2 + dmag(M,kx-x,ky-y)**3)**2 - 2*d2(kx-x,ky-y)*(dmag(M,kx-x,ky-y)+(vs*np.sqrt(x**2+y**2)))*dkx_dmag(M,kx-x,ky-y)/(omega**2+(dmag(M,kx-x,ky-y)+(vs*np.sqrt(x**2+y**2)))**2)**2)
    dkx_Im_Sigma_AB, err = integrate.dblquad(f11, -np.pi, np.pi, lambda x: -np.pi, lambda x: np.pi)
    
    #∂ky(Im[Sigma_AB]):
    f12 = lambda y, x: 1/(4*np.pi**2)*Mofq(x,y,u)**2*((vs*np.sqrt(x**2+y**2))*np.cos(ky-y)/((omega**2+(vs*np.sqrt(x**2+y**2))**2)*dmag(M,kx-x,ky-y) + 2*(vs*np.sqrt(x**2+y**2))*dmag(M,kx-x,ky-y)**2 + dmag(M,kx-x,ky-y)**3) + (vs*np.sqrt(x**2+y**2))*d2(kx-x,ky-y)*((omega**2+(vs*np.sqrt(x**2+y**2))**2)*dkx_dmag(M,kx-x,ky-y) + 4*(vs*np.sqrt(x**2+y**2))*dmag(M,kx-x,ky-y)*dkx_dmag(M,kx-x,ky-y) + 3*dmag(M,kx-x,ky-y)**2*dkx_dmag(M,kx-x,ky-y))/((omega**2+(vs*np.sqrt(x**2+y**2))**2)*dmag(M,kx-x,ky-y) + 2*(vs*np.sqrt(x**2+y**2))*dmag(M,kx-x,ky-y)**2 + dmag(M,kx-x,ky-y)**3)**2 + np.cos(ky-y)/(omega**2+(dmag(M,kx-x,ky-y)+(vs*np.sqrt(x**2+y**2)))**2) - 2*d2(kx-x,ky-y)*(dmag(M,kx-x,ky-y)+(vs*np.sqrt(x**2+y**2)))*dkx_dmag(M,kx-x,ky-y)/(omega**2+(dmag(M,kx-x,ky-y)+(vs*np.sqrt(x**2+y**2)))**2)**2)
    dky_Im_Sigma_AB, err = integrate.dblquad(f12, -np.pi, np.pi, lambda x: -np.pi, lambda x: np.pi)
    
    return domega_Sigma_AApBB, dkx_Sigma_AApBB, dky_Sigma_AApBB, domega_Re_Sigma_AB, dkx_Re_Sigma_AB, dky_Re_Sigma_AB, domega_Im_Sigma_AB, dkx_Im_Sigma_AB, dky_Im_Sigma_AB, domega_Sigma_AAmBB, dkx_Sigma_AAmBB, dky_Sigma_AAmBB
######################################################################################
######################################################################################
def dprime0(M, u, vs, kx, ky, omega):
    """
        Assembles zero-th component (proportional to the identity) of vector Hamiltonian after self-energy renormalization.
    
        Parameters
        ----------
        M: float, mass term in the Hamiltonian.
        
        u: float, acoustic phonon coupling constant.

        vs: float, sound velocity.

        kx: float, momentum in x direction
        
        ky: float, momentum in y direction.

        Returns
        -------
        dprime0: float, zeroth component of renormalized vector Hamiltonian.
        
        
        References
        ----------
        W. Chen, Weakly interacting topological insulators:
        Quantum criticality and the renormalization group approach,
        PHYSICAL REVIEW B 97, 115130 (2018).

        Notes
        -----

        Examples
        --------

    """
    dprime0 = -0.5*get_self_energies(M, u, vs, kx, ky, omega)[1]
    return dprime0
######################################################################################
######################################################################################
def dprime1(M, u, vs, kx, ky, omega):
    """
        Assembles first ("x") component of vector Hamiltonian after self-energy renormalization.
    
        Parameters
        ----------
        M: float, mass term in the Hamiltonian.
        
        u: float, acoustic phonon coupling constant.

        vs: float, sound velocity.

        kx: float, momentum in x direction
        
        ky: float, momentum in y direction.

        Returns
        -------
        dprime1: float, first component of renormalized vector Hamiltonian.
        
        
        References
        ----------
        W. Chen, Weakly interacting topological insulators:
        Quantum criticality and the renormalization group approach,
        PHYSICAL REVIEW B 97, 115130 (2018).

        Notes
        -----

        Examples
        --------

    """
    dprime1 = np.sin(kx) + get_self_energies(M, u, vs, kx, ky, omega)[2]
    return dprime1
######################################################################################
######################################################################################
def dprime2(M, u, vs, kx, ky, omega):
    """
        Assembles second ("y") component of vector Hamiltonian after self-energy renormalization.
    
        Parameters
        ----------
        M: float, mass term in the Hamiltonian.
        
        u: float, acoustic phonon coupling constant.

        vs: float, sound velocity.

        kx: float, momentum in x direction
        
        ky: float, momentum in y direction.

        Returns
        -------
        dprime2: float, second component of renormalized vector Hamiltonian.
        
        
        References
        ----------
        W. Chen, Weakly interacting topological insulators:
        Quantum criticality and the renormalization group approach,
        PHYSICAL REVIEW B 97, 115130 (2018).

        Notes
        -----

        Examples
        --------

    """
    dprime2 = np.sin(ky) - get_self_energies(M, u, vs, kx, ky, omega)[3]
    return dprime2
######################################################################################
######################################################################################
def dprime3(M, u, vs, kx, ky, omega):
    """
        Assembles third ("z") component of vector Hamiltonian after self-energy renormalization.
    
        Parameters
        ----------
        M: float, mass term in the Hamiltonian.
        
        u: float, acoustic phonon coupling constant.

        vs: float, sound velocity.

        kx: float, momentum in x direction
        
        ky: float, momentum in y direction.

        Returns
        -------
        dprime3: float, third component of renormalized vector Hamiltonian.
        
        
        References
        ----------
        W. Chen, Weakly interacting topological insulators:
        Quantum criticality and the renormalization group approach,
        PHYSICAL REVIEW B 97, 115130 (2018).

        Notes
        -----

        Examples
        --------

    """
    dprime3 = (M + 2 - np.cos(kx) - np.cos(ky)) + 0.5*get_self_energies(M, u, vs, kx, ky, omega)[0]
    return dprime3
######################################################################################
######################################################################################
def dprimevec(M, u, vs, kx, ky, omega):
    """
        Assembles the Hamiltonian unit vector after self-energy renormalization.
    
        Parameters
        ----------
        M: float, mass term in the Hamiltonian.
        
        u: float, acoustic phonon coupling constant.

        vs: float, sound velocity.

        kx: float, momentum in x direction
        
        ky: float, momentum in y direction.
        
        omega: float, Matsubara frequency.
        

        Returns
        -------
        dprimevec: float, Hamiltonian unit vector after self-energy renormalization.
        
        
        References
        ----------
        W. Chen, Weakly interacting topological insulators:
        Quantum criticality and the renormalization group approach,
        PHYSICAL REVIEW B 97, 115130 (2018).

        Notes
        -----

        Examples
        --------

    """
    d1 = dprime1(M, u, vs, kx, ky, omega)
    d2 = dprime2(M, u, vs, kx, ky, omega)
    d3 = dprime3(M, u, vs, kx, ky, omega)
    dprime_mag = np.sqrt(d1**2 + d2**2 + d3**2)
    
    dprimevec = np.array([d1,d2,d3])
    
    return dprimevec, dprime_mag
######################################################################################


######################################################################################
def curv_func(M, u, vs, kx, ky, omega):
    """
        Assembles the curvature function.
    
        Parameters
        ----------
        M: float, mass term in the Hamiltonian.
        
        u: float, acoustic phonon coupling constant.
        
        vs: float, optical phonon frequency.

        kx: float, momentum in x direction
        
        ky: float, momentum in y direction.
        
        omega: float, Matsubara frequency.
        

        Returns
        -------
        curv_func: float, curvature function at kx, ky for given parameters
        
        
        References
        ----------
        W. Chen, Weakly interacting topological insulators:
        Quantum criticality and the renormalization group approach,
        PHYSICAL REVIEW B 97, 115130 (2018).

        Notes
        -----
        
        d0' = -0.5*(Sigma_AA+Sigma_BB)
        d1' = d1 + Re[Sigma_AB]
        d2' = d2 - Im[Sigma_AB]
        d3' = d3 + 0.5*(Sigma_AA-Sigma_BB)
        
        Sigma_AA and Sigma_BB are independent of k.

        Examples
        --------

    """
    # Obtain Hamiltonian vector after self-energy renormalisation:
    dp0 = dprime0(M, u, vs, kx, ky, omega)
    dp1 = dprime1(M, u, vs, kx, ky, omega)
    dp2 = dprime2(M, u, vs, kx, ky, omega)
    dp3 = dprime3(M, u, vs, kx, ky, omega)

    # All components for omega, kx, and ky derivatives:
    dp0_domega = -0.5*get_self_energies_derivatives(M, u, vs, kx, ky, omega)[0]
    dp0_dkx = -0.5*get_self_energies_derivatives(M, u, vs, kx, ky, omega)[1]
    dp0_dky = -0.5*get_self_energies_derivatives(M, u, vs, kx, ky, omega)[2]

    dp1_domega = get_self_energies_derivatives(M, u, vs, kx, ky, omega)[3]
    dp1_dkx = np.cos(kx)+get_self_energies_derivatives(M, u, vs, kx, ky, omega)[4]
    dp1_dky = get_self_energies_derivatives(M, u, vs, kx, ky, omega)[5]
    
    dp2_domega = -get_self_energies_derivatives(M, u, vs, kx, ky, omega)[6]
    dp2_dkx = -get_self_energies_derivatives(M, u, vs, kx, ky, omega)[7]
    dp2_dky = np.cos(ky)-get_self_energies_derivatives(M, u, vs, kx, ky, omega)[8]
    
    dp3_domega = 0.5*get_self_energies_derivatives(M, u, vs, kx, ky, omega)[9]
    dp3_dkx = np.sin(kx)+0.5*get_self_energies_derivatives(M, u, vs, kx, ky, omega)[10]
    dp3_dky = np.sin(ky)+0.5*get_self_energies_derivatives(M, u, vs, kx, ky, omega)[11]


    # Put everything together to obtain the curvature function:
    curv_func = -1j*(dp1*dp2_dkx*dp3_dky + dp2*dp3_dkx*dp1_dky + dp3*dp1_dkx*dp2_dky - dp1*dp3_dkx*dp2_dky - dp3*dp2_dkx*dp1_dky - dp2*dp1_dkx*dp3_dky) + 1j*omega*(dp1_domega*dp2_dkx*dp3_dky + dp2_domega*dp3_dkx*dp1_dky + dp3_domega*dp1_dkx*dp2_dky - dp1_domega*dp3_dkx*dp2_dky - dp3_domega*dp2_dkx*dp1_dky - dp2_domega*dp1_dkx*dp3_dky) + (dp0*dp1_domega*dp2_dkx*dp3_dky + dp1*dp2_domega*dp3_dkx*dp0_dky + dp2*dp3_domega*dp0_dkx*dp1_dky + dp3*dp0_domega*dp1_dkx*dp2_dky + dp1*dp0_domega*dp3_dkx*dp2_dky + dp0*dp3_domega*dp2_dkx*dp1_dky + dp3*dp2_domega*dp1_dkx*dp0_dky + dp2*dp1_domega*dp0_dkx*dp3_dky + dp1*dp2_domega*dp0_dkx*dp3_dky + dp2*dp0_domega*dp3_dkx*dp1_dky + dp0*dp3_domega*dp1_dkx*dp2_dky + dp3*dp1_domega*dp2_dkx*dp0_dky - dp0*dp1_domega*dp3_dkx*dp1_dky - dp1*dp3_domega*dp2_dkx*dp0_dky - dp3*dp2_domega*dp0_dkx*dp1_dky - dp2*dp0_domega*dp1_dkx*dp3_dky - dp1*dp0_domega*dp2_dkx*dp3_dky - dp0*dp2_domega*dp3_dkx*dp1_dky - dp2*dp3_domega*dp1_dkx*dp0_dky - dp3*dp1_domega*dp0_dkx*dp2_dky - dp0*dp2_domega*dp1_dkx*dp3_dky - dp2*dp1_domega*dp3_dkx*dp0_dky - dp1*dp3_domega*dp0_dkx*dp2_dky - dp3*dp0_domega*dp2_dkx*dp1_dky)
    # Normalization:
    curv_func = 4*np.pi*1j/((1j*omega+dp0)**2 - dprimevec(M, u, vs, kx, ky, omega)[1]**2)**2*curv_func
    
    return curv_func
######################################################################################

######################################################################################
def curv_func_simplified(M, u, vs, kx, ky, omega):
    """
        Assembles the curvature function by exploiting the fact that some self-energies are zero (faster version).
    
        Parameters
        ----------
        M: float, mass term in the Hamiltonian.
        
        u: float, acoustic phonon coupling constant.

        vs: float, sound velocity.

        kx: float, momentum in x direction
        
        ky: float, momentum in y direction.
        
        omega: float, Matsubara frequency.
        

        Returns
        -------
        curv_func: float, curvature function at kx, ky for given parameters
        
        
        References
        ----------
        W. Chen, Weakly interacting topological insulators:
        Quantum criticality and the renormalization group approach,
        PHYSICAL REVIEW B 97, 115130 (2018).

        Notes
        -----
        
        d0' = -0.5*(Sigma_AA+Sigma_BB)
        d1' = d1 + Re[Sigma_AB]
        d2' = d2 - Im[Sigma_AB]
        d3' = d3 + 0.5*(Sigma_AA-Sigma_BB)
        
        Sigma_AA and Sigma_BB are independent of k.

        Examples
        --------

    """

    # Threshold for the calculation of certain integrals that are not very well behaved around the boundaries:
    thresh=10**(-8)
        
    #Sigma_AA-Sigma_BB = -2/(4pi^2) int M^2(q) d3(k-q)/d(k-q) [((vs*np.sqrt(x**2+y**2)) + d(k-q))/ (omega^2 - (d(k-q)+(vs*np.sqrt(x**2+y**2)))^2) ]  dq
    f1 = lambda y, x: -2/(4*np.pi**2)*Mofq(x,y,u)**2*d3(M,kx-x,ky-y)/dmag(M,kx-x,ky-y)*((vs*np.sqrt(x**2+y**2)) + dmag(M,kx-x,ky-y))/(omega**2 + (dmag(M,kx-x,ky-y) + (vs*np.sqrt(x**2+y**2)))**2)
    Sigma_AAmBB1, err = integrate.dblquad(f1, -np.pi, -thresh, lambda x: -np.pi, lambda x: -thresh)
    Sigma_AAmBB2, err = integrate.dblquad(f1, thresh, np.pi, lambda x: -np.pi, lambda x: -thresh)
    Sigma_AAmBB3, err = integrate.dblquad(f1, thresh, np.pi, lambda x: thresh, lambda x: np.pi)
    Sigma_AAmBB4, err = integrate.dblquad(f1, -np.pi, -thresh, lambda x: thresh, lambda x: np.pi)
    Sigma_AAmBB = Sigma_AAmBB1 + Sigma_AAmBB2 + Sigma_AAmBB3 + Sigma_AAmBB4
    dp3 = d3(M,kx,ky) + 0.5*(Sigma_AAmBB)
    
    #∂omega(Sigma_AA+Sigma_BB):
    f2 = lambda y, x: 2/(4*np.pi**2)*Mofq(x,y,u)**2*(-omega**2 + (dmag(M,kx-x,ky-y) + vs*np.sqrt(x**2+y**2))**2)/(omega**2 + (dmag(M,kx-x,ky-y) + vs*np.sqrt(x**2+y**2))**2)**2
    domega_Sigma_AApBB, err = integrate.dblquad(f2, -np.pi, np.pi, lambda x: -np.pi, lambda x: np.pi)
    dp0_domega = 1j*domega_Sigma_AApBB

    #∂kx(Re[Sigma_AB]):
    f3 = lambda y, x: -1/(4*np.pi**2)*Mofq(x,y,u)**2*((vs*np.sqrt(x**2+y**2))*np.cos(kx-x)/((omega**2+(vs*np.sqrt(x**2+y**2))**2)*dmag(M,kx-x,ky-y) + 2*(vs*np.sqrt(x**2+y**2))*dmag(M,kx-x,ky-y)**2 + dmag(M,kx-x,ky-y)**3) - (vs*np.sqrt(x**2+y**2))*d1(kx-x,ky-y)*((omega**2+(vs*np.sqrt(x**2+y**2))**2)*dkx_dmag(M,kx-x,ky-y) + 4*(vs*np.sqrt(x**2+y**2))*dmag(M,kx-x,ky-y)*dkx_dmag(M,kx-x,ky-y) + 3*dmag(M,kx-x,ky-y)**2*dkx_dmag(M,kx-x,ky-y))/((omega**2+(vs*np.sqrt(x**2+y**2))**2)*dmag(M,kx-x,ky-y) + 2*(vs*np.sqrt(x**2+y**2))*dmag(M,kx-x,ky-y)**2 + dmag(M,kx-x,ky-y)**3)**2 + np.cos(kx-x)/(omega**2+(dmag(M,kx-x,ky-y)+(vs*np.sqrt(x**2+y**2)))**2) - 2*d1(kx-x,ky-y)*(dmag(M,kx-x,ky-y)+(vs*np.sqrt(x**2+y**2)))*dkx_dmag(M,kx-x,ky-y)/(omega**2+(dmag(M,kx-x,ky-y)+(vs*np.sqrt(x**2+y**2)))**2)**2)
    dkx_Re_Sigma_AB, err = integrate.dblquad(f3, -np.pi, np.pi, lambda x: -np.pi, lambda x: np.pi)
    dp1_dkx = np.cos(kx) + dkx_Re_Sigma_AB
    
    #∂ky(Im[Sigma_AB]):
    f4 = lambda y, x: 1/(4*np.pi**2)*Mofq(x,y,u)**2*((vs*np.sqrt(x**2+y**2))*np.cos(ky-y)/((omega**2+(vs*np.sqrt(x**2+y**2))**2)*dmag(M,kx-x,ky-y) + 2*(vs*np.sqrt(x**2+y**2))*dmag(M,kx-x,ky-y)**2 + dmag(M,kx-x,ky-y)**3) - (vs*np.sqrt(x**2+y**2))*d2(kx-x,ky-y)*((omega**2+(vs*np.sqrt(x**2+y**2))**2)*dky_dmag(M,kx-x,ky-y) + 4*(vs*np.sqrt(x**2+y**2))*dmag(M,kx-x,ky-y)*dky_dmag(M,kx-x,ky-y) + 3*dmag(M,kx-x,ky-y)**2*dkx_dmag(M,kx-x,ky-y))/((omega**2+(vs*np.sqrt(x**2+y**2))**2)*dmag(M,kx-x,ky-y) + 2*(vs*np.sqrt(x**2+y**2))*dmag(M,kx-x,ky-y)**2 + dmag(M,kx-x,ky-y)**3)**2 + np.cos(ky-y)/(omega**2+(dmag(M,kx-x,ky-y)+(vs*np.sqrt(x**2+y**2)))**2) - 2*d2(kx-x,ky-y)*(dmag(M,kx-x,ky-y)+(vs*np.sqrt(x**2+y**2)))*dky_dmag(M,kx-x,ky-y)/(omega**2+(dmag(M,kx-x,ky-y)+(vs*np.sqrt(x**2+y**2)))**2)**2)
    dky_Im_Sigma_AB1, err = integrate.dblquad(f4, -np.pi, np.pi, lambda x: -np.pi, lambda x: np.pi)
    dp2_dky = np.cos(ky) - dky_Im_Sigma_AB

    curv_func = 4*np.pi*1j/(np.abs(dp3))**4*(-1j*dp3*dp1_dkx*dp2_dky + dp3*dp0_domega*dp1_dkx*dp2_dky)
    
    return curv_func
######################################################################################

######################################################################################
def curv_func_bare(M, kx, ky):
    """
        Assembles the curvature function without interactions.
    
        Parameters
        ----------
        M: float, mass term in the Hamiltonian.
        
        kx: float, momentum in x direction
        
        ky: float, momentum in y direction.

        Returns
        -------
        curv_func: float, curvature function at kx, ky for given parameters
        
        
        References
        ----------
        W. Chen, Weakly interacting topological insulators:
        Quantum criticality and the renormalization group approach,
        PHYSICAL REVIEW B 97, 115130 (2018).

        Notes
        -----

        Examples
        --------

    """
    d1 = np.sin(kx);
    d2 = np.sin(ky);
    d3 = M + 2 - np.cos(kx) - np.cos(ky)
    dmag = np.sqrt(d1**2 + d2**2 + d3**2)
    dxd1 = np.cos(kx)
    dyd2 = np.cos(ky)
    dxd3 = np.sin(kx)
    dyd3 = np.sin(ky)
    curv_func_bare = 1/(4*np.pi)*(1/dmag**3)*(-d1*dxd3*dyd2 - d2*dxd1*dyd3 + d3*dxd1*dyd2)
       
    return curv_func_bare
######################################################################################
