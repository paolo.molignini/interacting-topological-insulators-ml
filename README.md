# Interacting Topological Insulators ML

This repository contains a small set of python scripts to perform supervised classification of interacting topological insulators with the keras library.

When using this code, please cite the corresponding paper: P. Molignini et al, arXiv:2104.11237 (2021). URL: https://arxiv.org/abs/2104.11237. Please refer to the license file for the correct use and distribution of open-source codes under the GNU general public license.

**System requirements:**

You need to have python installed on your system for this program to run. This program was optimised for the following python packages:
- python 3.7
- numpy 1.21.0
- scipy 1.7.0
- matplotlib 3.4.2
- keras 2.4.3 (you might need a tensorflow2 backend for this to run properly)

We do not guarantee it will work with different versions.

**Structure of the repository:**

The source code is divided into two main folders labeled "SSH" and "Chern", which contain a series of modules used to assemble and compute the quantities used as input data (the value of the curvature function at the high-symmetry points), to perform the machine learning tasks, and to visualize the results. The folder "data" contains data generated during the computations. The scripts will also generate additional folders containing the trained neural networks (eg. "trained_model-M--6.0-2.0-1024-rand"). 

A working example for each type of topological insulator (SSH or Chern) is contained in the jupyer notebooks ("SSH_example.ipynb" and "Chern_example.ipynb"). You can simply execute the list of commands in the predefined notebooks to run the program. Alternatively, you can write your own python module or jupyter notebook to play around with the code. 
